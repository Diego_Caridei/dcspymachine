#!/bin/bash
##############################################################################
#  Questo  script  permette di monitorare la propria macchina inviando degli #     
#  screen shot via ftp su un vostro spazio web.				     #	
#  									     #	
#  Author   : Diego Caridei Aka Flexkid 				     #	 
#  Year     : 2014							     #
#  Version 	: 0.9 							     #
##############################################################################  
disclamer="DISCLAMER LEGALE: l'autore non è responsabile dell'eventuale uso improprio del presente software 
   o dei danni/disagi/malfunzionamenti causati dal suo utilizzo."


a=0
b=0
HOST="hostName"
USER="Username"
PASS="password"

function sendViaFtp {
ftp -n -i $HOST <<EOF
user $USER $PASS
binary
passive on
put image.zip
quit
EOF
}

while [ $a -le 10 ] ; do
	mkdir image

	 if [[ $OSTYPE == "linux-gnueabi" || $OSTYPE == "linux-gnu" ]]; then
      	import -frame $b.png   
     
 	else
		screencapture -x $b.png  
fi
	
	((b+=1))
	cp -v *.png  image
	rm -v *.png		
	sleep 2
	zip -r image.zip  image

	sendViaFtp
	rm image.zip
	rm -r -f image

done
